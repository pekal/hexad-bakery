import com.hexad.bakery.Config;
import com.hexad.bakery.entity.Package;
import com.hexad.bakery.entity.Product;
import com.hexad.bakery.util.ApplicationException;
import com.hexad.bakery.util.Helper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HelperTest {
    private static List<Product> productList;

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
        Config config = new Config();
        productList = config.getProdcuts();
    }

    @Test
    void testGetProductOk() {
        Product product = productList.get(0);
        Helper helper = new Helper();
        Product searchedProduct = helper.getProduct("VS5", productList);
        assertEquals(product, searchedProduct);
    }

    @Test
    void testGetProductNull() {
        Helper helper = new Helper();
        Product searchedProduct = helper.getProduct("TEST", productList);
        assertNull(searchedProduct);
    }

    @Test
    void testCorrectParams() throws ApplicationException {
        String args[] = new String[]{"10", "VS5"};
        Helper helper = new Helper();
        assertTrue(helper.validationInputData(args, productList));
    }

    @Test
    void testWrongCountParams() throws ApplicationException {
        assertThrows(ApplicationException.class, () -> {
            String args[] = new String[]{"10", "VS5", "11"};
            Helper helper = new Helper();
            helper.validationInputData(args, productList);
        });
    }

    @Test
    void testWrongFirstParams() throws ApplicationException {
        assertThrows(ApplicationException.class, () -> {
            String args[] = new String[]{"test", "VS5"};
            Helper helper = new Helper();
            helper.validationInputData(args, productList);
        });
    }

    @Test
    void testNullProductList() throws ApplicationException {
        assertThrows(ApplicationException.class, () -> {
            String args[] = new String[]{"10", "VS5"};
            Helper helper = new Helper();
            helper.validationInputData(args, null);
        });
    }

    @Test
    void testEmptyProductList() throws ApplicationException {
        assertThrows(ApplicationException.class, () -> {
            String args[] = new String[]{"10", "VS5"};
            Helper helper = new Helper();
            helper.validationInputData(args, new ArrayList<Product>());
        });
    }

    @Test
    void showResultForVs5() throws ApplicationException {
        Helper helper = new Helper();
        String args[] = new String[]{"10", "VS5X"};
        assertEquals("10 VS5X 17.98 USD\n2 x 5 8.99 USD", helper.showResult(args, productList));
    }

    @Test
    void testExampleVs5() throws ApplicationException {
        List<Package> packageList = new ArrayList<>();
        packageList.add(productList.get(0).getPackagesList().get(1));
        packageList.add(productList.get(0).getPackagesList().get(1));
        Helper helper = new Helper();
        assertEquals(packageList, helper.packages(productList.get(0), 10));
    }

    @Test
    void testExampleMb11() throws ApplicationException {
        List<Package> packageList = new ArrayList<>();
        packageList.add(productList.get(1).getPackagesList().get(2));
        packageList.add(productList.get(1).getPackagesList().get(0));
        packageList.add(productList.get(1).getPackagesList().get(0));
        packageList.add(productList.get(1).getPackagesList().get(0));
        Helper helper = new Helper();
        assertEquals(packageList, helper.packages(productList.get(1), 14));
    }

    @Test
    void testExampleCF() throws ApplicationException {
        List<Package> packageList = new ArrayList<>();
        packageList.add(productList.get(2).getPackagesList().get(1));
        packageList.add(productList.get(2).getPackagesList().get(1));
        packageList.add(productList.get(2).getPackagesList().get(0));
        Helper helper = new Helper();
        assertEquals(packageList, helper.packages(productList.get(2), 13));
    }
}
