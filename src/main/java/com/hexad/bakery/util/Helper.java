package com.hexad.bakery.util;

import com.hexad.bakery.entity.Package;
import com.hexad.bakery.entity.Product;
import com.hexad.bakery.entity.SortPackage;

import java.util.*;
import java.util.stream.Collectors;

public class Helper {

    public Product getProduct(String code, List<Product> productList) {
        return productList.stream()
                .filter(product -> code.equalsIgnoreCase(product.getCode()))
                .findFirst()
                .orElse(null);
    }

    public boolean validationInputData(String args[], List<Product> productList) throws ApplicationException {
        if (productList == null || productList.isEmpty()) {
            throw new ApplicationException("product list is empty");
        }
        if (args.length != 2) {
            throw new ApplicationException("wrong counts of input arguments");
        }
        try {
            Integer.parseInt(args[0]);
        } catch (NumberFormatException numberException) {
            throw new ApplicationException("wrong value of first input arguments");
        }
        return true;
    }

    public List<Package> packages(Product product, int quantity) throws ApplicationException {
        List<Package> result = new ArrayList<>();
        int actualValue = 0;
        List<Package> sortedPackages = product.getPackagesList();
        sortedPackages.sort(new SortPackage());

        Package ignorePackage = null;
        while (quantity > actualValue) {
            for (Package p : sortedPackages) {
                while (p.getQuantity() < (quantity - actualValue) && !p.equals(ignorePackage)) {
                    result.add(p);
                    actualValue += p.getQuantity();
                }
                if (p.getQuantity() == (quantity - actualValue)) {
                    result.add(p);
                    return result;
                }
            }
            if (result.size() == 0) {
                throw new ApplicationException("not possible to find good set");
            }
            if (quantity != actualValue) {
                ignorePackage = result.get(result.size() - 1);
                result.remove(result.size() - 1);
                actualValue -= ignorePackage.getQuantity();
            }
        }
        return result;
    }

    public String showResult(String args[], List<Product> productList) throws ApplicationException {
        this.validationInputData(args, productList);
        Product product = this.getProduct(args[1], productList);
        if (product == null) {
                throw new ApplicationException("no product");
        }
        Integer number = Integer.valueOf(args[0]);
        List<Package> packageList= this.packages(product, number);
        Map<String, Long> countingMap = packageList.stream().collect(
                Collectors.groupingBy(Package::getLabel, Collectors.counting()));

        Double sum = 0.00;
        Currency currency = Currency.getInstance("USD");
        for (Package p : packageList) {
            if (!currency.equals(p.getCurrency())) {
                throw new ApplicationException("we can use only one currency");
            }
            sum += p.getPrice();
        }

        String result = args[0] + " " + args[1] + " " + sum + " " + currency.getSymbol();
        for (Map.Entry<String, Long> entry : countingMap.entrySet()) {
            result = result.concat("\n" + entry.getValue() + " x " + entry.getKey());
        }
        return  result;
    }

}
