package com.hexad.bakery.util;

public class ApplicationException extends Exception {

    public ApplicationException(String message) {
        super(message);
    }
}
