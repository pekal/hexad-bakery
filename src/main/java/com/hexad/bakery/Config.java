package com.hexad.bakery;

import com.hexad.bakery.entity.Package;
import com.hexad.bakery.entity.Product;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

public final class Config {
    private final List<Product> prodcuts;

    public Config() {
        prodcuts = new ArrayList<Product>();
        List<Package> packagesVegemiteScroll = new ArrayList<Package>();
        packagesVegemiteScroll.add(new Package(3, 6.99, Currency.getInstance("USD")));
        packagesVegemiteScroll.add(new Package(5, 8.99, Currency.getInstance("USD")));
        Product productVegemiteScroll = new Product("Vegemite Scroll", "VS5", packagesVegemiteScroll);

        List<Package> packagesBlueberryMuffin = new ArrayList<Package>();
        packagesBlueberryMuffin.add(new Package(2, 9.95, Currency.getInstance("USD")));
        packagesBlueberryMuffin.add(new Package(5, 16.95, Currency.getInstance("USD")));
        packagesBlueberryMuffin.add(new Package(8, 24.95, Currency.getInstance("USD")));
        Product productBlueberryMuffin = new Product("Blueberry Muffin", "MB11", packagesBlueberryMuffin);

        List<Package> packagesCroissant = new ArrayList<Package>();
        packagesCroissant.add(new Package(3, 5.95, Currency.getInstance("USD")));
        packagesCroissant.add(new Package(5, 9.95, Currency.getInstance("USD")));
        packagesCroissant.add(new Package(9, 16.99, Currency.getInstance("USD")));
        Product productCroissant = new Product("Croissant", "CF", packagesCroissant);

        List<Package> packagesVegemiteXScroll = new ArrayList<Package>();
        packagesVegemiteXScroll.add(new Package(3, 6.99, Currency.getInstance("USD")));
        packagesVegemiteXScroll.add(new Package(5, 8.99, Currency.getInstance("USD")));
        Product productVegemiteXScroll = new Product("Vegemite Scroll X", "VS5X", packagesVegemiteXScroll);

        prodcuts.add(productVegemiteScroll);
        prodcuts.add(productBlueberryMuffin);
        prodcuts.add(productCroissant);
        prodcuts.add(productVegemiteXScroll);
    }

    public List<Product> getProdcuts() {
        return prodcuts;
    }
}
