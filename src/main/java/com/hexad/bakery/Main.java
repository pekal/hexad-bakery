package com.hexad.bakery;

import com.hexad.bakery.util.ApplicationException;
import com.hexad.bakery.util.Helper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Main {
    public static void main(String args[]) {
        if (args.length == 0) {
            System.out.println("First param should be link to input file e.g /test/input");
            System.exit(1);
        }
        String fileName = args[0];
        Config config = new Config();
        Helper helper = new Helper();
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(item -> {
                String oneLineArrays[] = item.split(" ");
                try {
                    System.out.println(helper.showResult(oneLineArrays, config.getProdcuts()));
                } catch (ApplicationException e) {
                    System.out.println(e.getMessage());
                }
            });
        } catch (IOException e) {
            System.out.println("file does not exist");
        }
    }
}
