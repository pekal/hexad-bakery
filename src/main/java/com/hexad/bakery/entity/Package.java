package com.hexad.bakery.entity;

import java.util.Currency;

public class Package {
    private int quantity;
    private Double price;
    private Currency currency;

    public Package(int quantity, Double price, Currency currency) {
        this.quantity = quantity;
        this.price = price;
        this.currency = currency;
    }

    public int getQuantity() {
        return quantity;
    }

    public Double getPrice() {
        return price;
    }

    public Currency getCurrency() {
        return currency;
    }

    public String getLabel() {
        return quantity + " " + price + " " + currency.getSymbol();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Package)) return false;

        Package aPackage = (Package) o;

        if (quantity != aPackage.quantity) return false;
        if (!price.equals(aPackage.price)) return false;
        return currency.equals(aPackage.currency);
    }

    @Override
    public int hashCode() {
        int result = quantity;
        result = 31 * result + price.hashCode();
        result = 31 * result + currency.hashCode();
        return result;
    }
}
