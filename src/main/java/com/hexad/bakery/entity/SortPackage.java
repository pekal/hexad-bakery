package com.hexad.bakery.entity;

import java.util.Comparator;

public class SortPackage implements Comparator<Package> {

    @Override
    public int compare(Package o1, Package o2) {
        if (o2.getCurrency().equals(o1.getCurrency())) {
            return (int) (o2.getPrice() - o1.getPrice());
        }
        return 0;
    }
}
