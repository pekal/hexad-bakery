package com.hexad.bakery.entity;

import java.util.List;

public class Product {

    private String name;
    private String code;
    private List<Package> packagesList;

    public Product(String name, String code, List<Package> packagesList) {
        this.name = name;
        this.code = code;
        this.packagesList = packagesList;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public List<Package> getPackagesList() {
        return packagesList;
    }
}
